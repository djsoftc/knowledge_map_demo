# -*- coding: utf-8 -*-
from py2neo import Graph
from igraph import Graph as IGraph
from igraph import plot as plot
from py2neo import Graph,Node,Relationship,NodeSelector,Subgraph,DataList

import networkx as nx
import numpy as np
from node2vec import Node2Vec
p=[0.9,0.1]
J=[u'7331710182400002709', u'7440810182200001028']
K = len(J)
print K
kk = int(np.floor(np.random.rand()*K))
print kk
# if np.random.rand() < q[kk]:
#     return kk
# else:
#     return J[kk]



print np.random.choice([u'7331710182400002709', u'7440810182200001028'], size=1, p=[0.9,0.1])[0]

graph = Graph(password = "admin")
query = '''
MATCH (c1:Character)-[r:INTERACTS]->(c2:Character)
RETURN c1.name, c2.name, r.weight AS weight LIMIT 25
'''

query = '''
MATCH (n:ci_tran)-[r:ci_link_tran]->(m:ci_tran) 
RETURN n.name, m.name, r.tran_count AS weight LIMIT 100
'''
import datetime

begin = datetime.datetime.now()
edge_list = []
graphdata=graph.run(query)
for record in graphdata:
    edge_list.append(tuple(record))
print edge_list


ig = IGraph.TupleList(graph.run(query), weights=True)

# G = nx.DiGraph(A) # In case your graph is directed
G = nx.Graph(ig.get_edgelist()) # In case you graph is undirected
print G.number_of_edges()
cc=ig.community_label_propagation()
# dd=ig.community_walktrap(weights='weight').as_clustering()
print len(cc)
# print cc
# print dd
# layout = ig.layout("kk")
# plot(cc,layout = layout)
# plot(dd,layout = layout)
print datetime.datetime.now()-begin
# aa = nx.Graph()
# #G.add_nodes_from(id_tag)
# aa.add_edges_from(ig.get_adjedgelist())
#
#
# node2vec = Node2Vec(aa, dimensions=64, walk_length=30, num_walks=200, workers=1)



# pg = ig.pagerank()
# pgvs = []
# for p in zip(ig.vs, pg):
#     print(p)
#     pgvs.append({"name": p[0]["name"], "pg": p[1]})
# pgvs
# write_clusters_query = '''
# UNWIND {nodes} AS n
# MATCH (c:Character) WHERE c.name = n.name
# SET c.pagerank = n.pg
# '''
# graph.run(write_clusters_query, nodes=pgvs)

# clusters = IGraph.community_walktrap(ig, weights="weight").as_clustering()
# nodes = [{"name": node["name"]} for node in ig.vs]
# for node in nodes:
#     idx = ig.vs.find(name=node["name"]).index
#     node["community"] = clusters.membership[idx]
# write_clusters_query = '''
# UNWIND {nodes} AS n
# MATCH (c:Character) WHERE c.name = n.name
# SET c.community = toInt(n.community)
# '''
# graph.run(write_clusters_query, nodes=nodes)