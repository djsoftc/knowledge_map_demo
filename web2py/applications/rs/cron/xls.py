# -*- coding: utf-8 -*-
# 导入xlwt模块
import xlwt
from random import Random
import copy
import string
from datetime import *
import random
import time
import uuid
visaPrefixList = [
    ['6', '2', '2', '1'],
    ['6', '2', '3', '6'],
    ['6', '2', '4', '6'],
    ['6', '2', '5', '2'],
    ['6', '2', '6', '9'],
    ['6', '2', '7', '4', '0', '0', '7', '1'],
    ['6', '2', '8', '6'],
    ['6', '2', '9', '6'],
    ['6']]

def completed_number(prefix, length):
  """
  'prefix' is the start of the CC number as a string, any number of digits.
  'length' is the length of the CC number to generate. Typically 13 or 16
  """
  ccnumber = prefix
  # generate digits
  while len(ccnumber) < (length - 1):
    digit = str(generator.choice(range(0, 10)))
    ccnumber.append(digit)
  # Calculate sum
  sum = 0
  pos = 0
  reversedCCnumber = []
  reversedCCnumber.extend(ccnumber)
  reversedCCnumber.reverse()
  while pos < length - 1:
    odd = int(reversedCCnumber[pos]) * 2
    if odd > 9:
      odd -= 9
    sum += odd
    if pos != (length - 2):
      sum += int(reversedCCnumber[pos + 1])
    pos += 2
  # Calculate check digit
  checkdigit = ((sum / 10 + 1) * 10 - sum) % 10
  ccnumber.append(str(checkdigit))
  return ''.join(ccnumber)
def credit_card_number(rnd, prefixList, length, howMany):
  result = []
  while len(result) < howMany:
    ccnumber = copy.copy(rnd.choice(prefixList))
    result.append(completed_number(ccnumber, length))
  return result

def rangetime(): #print time.mktime((2016,1,1,0,0,0,0,0,0))
    nowTime = datetime.now().strftime("%Y%m%d%H%M%S")  # 生成当前的时间
    randomNum = random.randint(1451577600,1514735999)
    date_touple = time.localtime(randomNum)
    return time.strftime("%Y%m%d%H%M%S",date_touple)

def rangemac():
    Maclist = []
    for i in range(1,7):
        RANDSTR = "".join(random.sample("0123456789abcdef",2))
        Maclist.append(RANDSTR)
    RANDMAC = ":".join(Maclist)
    return RANDMAC

# 创建一个Workbook对象，这就相当于创建了一个Excel文件
book = xlwt.Workbook(encoding='utf-8', style_compression=0)
'''
Workbook类初始化时有encoding和style_compression参数
encoding:设置字符编码，一般要这样设置：w = Workbook(encoding='utf-8')，就可以在excel中输出中文了。
默认是ascii。当然要记得在文件头部添加：
#!/usr/bin/env python
# -*- coding: utf-8 -*-
style_compression:表示是否压缩，不常用。
'''
# 创建一个sheet对象，一个sheet对象对应Excel文件中的一张表格。
# 在电脑桌面右键新建一个Excel文件，其中就包含sheet1，sheet2，sheet3三张表
sheet = book.add_sheet('test', cell_overwrite_ok=True)
# 其中的test是这张表的名字,cell_overwrite_ok，表示是否可以覆盖单元格，其实是Worksheet实例化的一个参数，默认值是False
# 向表test中添加数据 '马可瓦多大师傅'.decode('utf-8')
list=['债账号','账户名称','客户账号类型','余额字段名称','明细序号',
      '借贷标志','交易币种','账户钞汇标志','交易金额','账户余额',
      '客户账号','子账户序号','是否打印','终端号','部提标志',
      '产品编号','产品所属对象','账户期限值','凭证种类','凭证批号',
      '凭证序号','摘要代码','摘要描述','交易渠道','外部交易码',
      '内部交易码','现转标志','对方客户账号','对方子账户序号','对方负债系统账号 ',
      '对方户名','对方客户类型','对方金融机构名称','对方金融机构类型','对方金融机构代码',
      '代理人姓名','代理人证件类型','代理人证件号码','代理人国籍','组合产品号 ',
      '组合产品账号','银行业务编号','相关业务编号','柜员流水号','交易营业机构',
      '账户开户机构','操作柜员','复核人','授权柜员','交易日期',
      '交易时间','主机日期','冲正标志','被冲正标志','错账原日期',
      '错账原柜员流水号','备注','交易触发方式','打印页数','法人代码',
      '维护柜员','维护机构','维护日期','维护时间','时间戳','记录状态','手机号']
for index, item in enumerate(list):
    #print index, item
    sheet.write(0, index, item)
num=1000
generator = Random()
generator.seed()    # Seed from current time
mastercard = credit_card_number(generator, visaPrefixList, 16, num)
mastercard_to = credit_card_number(generator, visaPrefixList, 16, num)

for i in range(1, num, 1):
    sheet.write(i, 0, mastercard[i-1])#债账号
    sheet.write(i, 1, 'xxxxxx')#账户名称
    sheet.write(i, 2, '1') #客户账号类型
    sheet.write(i, 3, 'LDGBAL') #余额字段名称
    sheet.write(i, 4, str(uuid.uuid4())) #明细序号
    sheet.write(i, 5, 0) #借贷标志
    sheet.write(i, 6, '01')#交易币种
    sheet.write(i, 7, '0')  # 账户钞汇标志
    sheet.write(i, 8, random.randint(1,10000)) #'交易金额'
    sheet.write(i, 9, random.randint(1,10000)) #'账户余额'
    sheet.write(i, 12, random.randint(0, 1))  # '是否打印'
    sheet.write(i, 13, rangemac())#终端号
    sheet.write(i, 14, random.randint(0, 1))  #部提标志
    sheet.write(i, 16, random.randint(0, 1))  #产品所属对象
    sheet.write(i, 18, '00'+str(random.randint(0, 9)))  #凭证种类
    sheet.write(i, 21, '1')  #摘要代码
    sheet.write(i, 22, '转账')  #摘要描述
    sheet.write(i, 23, '0'+str(random.randint(0, 3)))  #交易渠道
    sheet.write(i, 24, 'cd4001')  #外部交易码
    sheet.write(i, 25, '4001')  # 内部交易码
    sheet.write(i, 26, '1')  #现转标志
    sheet.write(i, 27, mastercard_to[i-1]) #对方客户账号
    sheet.write(i, 30, '*****')  #对方户名
    sheet.write(i, 31, '0'+str(random.randint(0, 4)))  #对方客户类型
    sheet.write(i, 33, '1' + str(random.randint(0, 4)))#对方金融机构类型
    sheet.write(i, 43, str(uuid.uuid1())) #柜员流水号
    sheet.write(i, 49, rangetime())  # 交易日期
    sheet.write(i, 51, rangetime())  #主机日期
    sheet.write(i, 57, random.randint(0, 1))  #交易触发方式
    sheet.write(i, 65, 0)#记录状态
    sheet.write(i, 66,random.choice(['139','188','185','136','158','151'])+"".join(random.choice("0123456789") for i in range(8)))
# 最后，将以上操作保存到指定的Excel文件中
book.save(r'test1.xls')  # 在字符串前加r，声明为raw字符串，这样就不会处理其中的转义了。否则，可能会报错