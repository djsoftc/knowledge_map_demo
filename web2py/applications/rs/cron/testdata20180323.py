# -*- coding: utf-8 -*-
from py2neo import Graph
from igraph import Graph as IGraph
from igraph import plot as plot
from py2neo import Graph,Node,Relationship,NodeSelector,Subgraph
# from py2neo.ext.batman.batch import WriteBatch
import random
graph = Graph(password = "admin")
nodes = {"a": Node("test0424", name="dja", age=13),
         "b": Node("test0424", name="djb"),
         "c": Node("test0424", name="djc"),
         "d": Node("test0424", name="djd"),
         "e": Node("test0424", name="dje"),
         "f": Node("test0424", name="djf"),
         "g": Node("test0424", name="djg"),
         "h": Node("test0424", name="djh"),
         "i": Node("test0424", name="dji"),
         "m": Node("test0424", name="djm"),
         "n": Node("test0424", name="djn")}
A = Subgraph(relationships=[Relationship(nodes.get("a"), "trans0424", nodes.get("b")),
                            Relationship(nodes.get("a"), "trans0424", nodes.get("c")),
Relationship(nodes.get("a"), "trans0424", nodes.get("d")),
Relationship(nodes.get("d"), "trans0424", nodes.get("e")),
Relationship(nodes.get("d"), "trans0424", nodes.get("f")),
Relationship(nodes.get("f"), "trans0424", nodes.get("g")),
Relationship(nodes.get("f"), "trans0424", nodes.get("h")),
Relationship(nodes.get("h"), "trans0424", nodes.get("i")),
Relationship(nodes.get("i"), "trans0424", nodes.get("m")),
Relationship(nodes.get("d"), "trans0424", nodes.get("m")),
                            Relationship(nodes.get("i"), "trans0424", nodes.get("n"))])
graph.create(A)

#community Detection算法很多、如：k-clique、label propagation 、fast unfolding、louvain等等等等
# query = '''
# MATCH (c1:test0424)-[r:trans0424]->(c2:test0424)
# RETURN c1.name, c2.name, '3' AS weight LIMIT 25
# '''
# ig = IGraph.TupleList(graph.run(query), weights=True)
# cc= ig.community_label_propagation()
# # cc=ig.community_multilevel()
# 
# print cc
# # for sg in cc.subgraphs(): # 遍历每个社区子图
# #     print plot(sg)
# layout = ig.layout("kk")
# plot(cc,layout = layout)
# plot(ig)
# print lpa(ig)