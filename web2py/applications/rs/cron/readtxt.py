# -*- coding: utf-8 -*-
from py2neo import Graph,Node,Relationship,NodeSelector,Subgraph
import time
import datetime
import multiprocessing
import sys
import copy
import logging
import linecache
reload(sys)
sys.setdefaultencoding('utf-8')
graph = Graph(password="admin")
filename = "/home/dhlsoft/uftp/cipsna.txt"


def func(listaa):
    lista=[]
    for num in listaa:
        # print num
        line=linecache.getline(filename,num)
        print num, line
        sn = line.split(chr(3))
        if len(sn) < 10:
            continue
        # print i, line

        # 1.cipsna 客户基本信息
        #     2 客户号 《-- 证件--》  4 证件类型 +5 证件号码 Customer
        rel = Relationship(Node("ci_customer", name=sn[1]), "ci_link_card",
                           Node("ci_idcard", name=str(sn[3]) + str(sn[4])),
                           name='idcard', )
        lista.append(rel)
        # time.sleep(3)
    A = Subgraph(relationships=lista)
    print graph.merge(A, None, ('name'))
if __name__ == "__main__":

    # filename='/home/dhlsoft/文档/知识图谱/baike_triples.txt'
    multiprocessing.log_to_stderr()
    logger = multiprocessing.get_logger()
    logger.setLevel(logging.INFO)
    pool = multiprocessing.Pool(processes=4)
    file = open(filename, 'rb')
    num=10
    msg=[]
    i=0
    print linecache.getline(filename, 0)

    for index,line in enumerate(file):
                i = i + 1
                msg.append(i)
                if i % num == (num - 1):
                    pool.apply_async(func, (msg,))
                    msg = []
                    # print  i, 'clear'
                    if i>35:
                        break
    # A=Subgraph(relationships=list)
    # graph.merge(A,None,('name'))
    # elapsed = (time.clock() - start)
    # print "use time :" + str(elapsed)
    # pool.apply_async(func, (copy.copy(lista),))
    # print  datetime.datetime.now()-begin
    pool.close()
    pool.join()
    # while True:
    #     pass