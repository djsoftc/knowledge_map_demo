# -*- coding: utf-8 -*-
import sys
from hdfs.client import Client
from py2neo import Graph,Node,Relationship,NodeSelector,Subgraph
import datetime
import multiprocessing
reload(sys)
sys.setdefaultencoding('utf-8')

graph = Graph(password = "admin")
# client = Client(url, root=None, proxy=None, timeout=None, session=None)
client = Client("http://127.0.0.1:50070")

pool = multiprocessing.Pool(processes=4)
def loop(lista):
    print len(lista)
    A = Subgraph(relationships=lista)
    graph.merge(A, None, ('name'))

# print list(client,'/')
i=0
list=[]
num=10
begin = datetime.datetime.now()
with client.read("/graph/cipsna.txt", encoding='utf-8', delimiter='\n') as reader:
    for line in reader:
        # pass
        i=i+1
        sn=line.strip().split(chr(3))
        if len(sn)<10:
            continue
        print i, line

        # 1.cipsna 客户基本信息
        #     2 客户号 《-- 证件--》  4 证件类型 +5 证件号码 Customer
        rel = Relationship(Node("ci_customer", name=sn[1]), "ci_link_card",
                           Node("ci_idcard", name=str(sn[3])+str(sn[4])),
                           name='idcard', )
        list.append(rel)

        if i%num==(num-1):
            pool.apply_async(loop, (list,))
            list=[]
            print  i,'clear'

        # lines.append(line.strip())
pool.apply_async(loop, (list,))
datetime.datetime.now()-begin
pool.close()
pool.join()