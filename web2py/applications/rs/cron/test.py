# -*- coding: utf-8 -*-
import multiprocessing
import time
from py2neo import Graph,Node,Relationship,NodeSelector,Subgraph
from py2neo.ext.batman.batch import WriteBatch

def worker_1(interval):
    print "worker_1"
    time.sleep(interval)
    print "end worker_1"

def worker_2(interval):
    print "worker_2"
    time.sleep(interval)
    print "end worker_2"

def worker_3(interval):
    print "worker_3"
    time.sleep(interval)
    print "end worker_3"

if __name__ == "__main__":
    # 连接neo4j数据库
    graph = Graph("http://127.0.0.1:7474", username = "neo4j", password = "admin")
    # graph = Graph(password="excalibur")
    print graph.data("MATCH (a:people) RETURN a")
    selector = NodeSelector(graph)
    selected = selector.select("zte")
    print list(selected)

    nodes = {"a": Node("zte5", name="weiyudang", age=13), "b": Node("zte5", name="wangjiaqi")}
    # rel_a = Relationship(nodes.get("a"), "likes", nodes.get("b"))
    # rel_b = Relationship(nodes.get("b"), "likes", nodes.get("a"))
    # A = Subgraph(relationships=[rel_a, rel_b])
    start = time.clock()

    for j in range(100):
        list = []
        for i in range(50000+10000*j,60000+10000*j,1):
            list.append(Node("zte_test", name="dj"+str(i), age=i,tel=i))
        start1 = time.clock()
        A = Subgraph(nodes=list)
        graph.create(A)
        elapsed = (time.clock() - start1)
        print "Time used1:" + str(elapsed)
    # www=WriteBatch(graph=graph)
    # www.create(Node("zteBatch", name="weiyudang", age=13))
    # www.
    elapsed1 = (time.clock() - start)
    print "Time used:" +str(elapsed1)
    # list=[]
    # list.append(Node("zte6", name="weiyudang", age=13))
    # list.append(Node("zte6", name="wangjiaqi"))
    # A=Subgraph(nodes=list)
    # graph.create(A)


    # a = Node("Person", name="Alice")
    # b = Node("Person", name="Bob")
    # ab = Relationship(a, "KNOWS", b)
    # print ab


    # 创建结点：label结点，方便以后的结点查找操作
    temp_node1 = Node(lable="zte3", name="node1")
    # temp_node2 = Node(lable="Person", name="node2")
    graph.create(temp_node1)
    # graph.create(temp_node2)
    # tx = graph.begin()
    # a = Node("zte", name="dhlsoft")
    # tx.create(a)
    # b = Node("zte", name="djsoft")
    # ab = Relationship(a, "同事", b)
    # tx.create(ab)
    # tx.commit()
    # print graph.exists(ab)

    # 建立关系
    # node_1_call_node_2 = Relationship(temp_node1, 'CALL', temp_node2)
    # node_1_call_node_2['count'] = 1
    # node_2_call_node_1 = Relationship(temp_node2, 'CALL', temp_node1)
    # graph.create(node_2_call_node_1)
    # graph.create(node_1_call_node_2)
    # # 更新关系或节点的属性 push提交
    # node_1_call_node_2['count'] += 1
    # graph.push(node_1_call_node_2)
    #
    # # 通过属性值来查找节点和关系find_one
    # find_code = graph.find_one(
    #     label="明教",
    #     property_key="name",
    #     property_value="张无忌"
    # )
    # # print(find_code['name'])
    # print find_code
    # find方法已被弃用：通过属性值来查找所有节点和关系find替换为：NodeSelector
    # find = NodeSelector(graph).select('明教')
    # for f in find:
    #     print(f['name'])