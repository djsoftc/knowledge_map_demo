# -*- coding: utf-8 -*-
import multiprocessing
import time
import logging
def func(msg):
  # for i in xrange(3):
  print msg
    # time.sleep(1)
  # return "done " + msg
if __name__ == "__main__":
  multiprocessing.log_to_stderr()
  logger = multiprocessing.get_logger()
  logger.setLevel(logging.INFO)
  pool = multiprocessing.Pool(processes=4)
  result = []
  for i in xrange(10):
    msg = "hello %d" %(i)
    pool.apply_async(func, (msg, ))
  pool.close()
  pool.join()
  # for res in result:
  #   print res.get()
  # print "Sub-process(es) done."