# -*- coding: utf-8 -*-
from py2neo import Graph
from igraph import Graph as IGraph
from igraph import plot as plot
from py2neo import Graph,Node,Relationship,NodeSelector,Subgraph
from Cython.Build import cythonize
import networkx as nx
from node2vec import Node2Vec
import gensim
import datetime

begin = datetime.datetime.now()
new_model = gensim.models.Word2Vec.load('bb')
print new_model.wv.most_similar('7110010117304003101')
print datetime.datetime.now()-begin

begin = datetime.datetime.now()
mode1l = gensim.models.KeyedVectors.load_word2vec_format('aa',binary=False)
print mode1l.wv.most_similar('7110010117304003101')
print datetime.datetime.now()-begin