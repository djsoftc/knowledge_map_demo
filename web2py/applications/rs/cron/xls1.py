# -*- coding: utf-8 -*-
# 导入xlwt模块
import xlwt
from random import Random
import copy
import string
from datetime import *
import random
import time
import csv

visaPrefixList = [
    ['6', '2', '2', '1'],
    ['6', '2', '3', '6'],
    ['6', '2', '4', '6'],
    ['6', '2', '5', '2'],
    ['6', '2', '6', '9'],
    ['6', '2', '7', '4', '0', '0', '7', '1'],
    ['6', '2', '8', '6'],
    ['6', '2', '9', '6'],
    ['6']]

def completed_number(prefix, length):
  """
  'prefix' is the start of the CC number as a string, any number of digits.
  'length' is the length of the CC number to generate. Typically 13 or 16
  """
  ccnumber = prefix
  # generate digits
  while len(ccnumber) < (length - 1):
    digit = str(generator.choice(range(0, 10)))
    ccnumber.append(digit)
  # Calculate sum
  sum = 0
  pos = 0
  reversedCCnumber = []
  reversedCCnumber.extend(ccnumber)
  reversedCCnumber.reverse()
  while pos < length - 1:
    odd = int(reversedCCnumber[pos]) * 2
    if odd > 9:
      odd -= 9
    sum += odd
    if pos != (length - 2):
      sum += int(reversedCCnumber[pos + 1])
    pos += 2
  # Calculate check digit
  checkdigit = ((sum / 10 + 1) * 10 - sum) % 10
  ccnumber.append(str(checkdigit))
  return ''.join(ccnumber)
def credit_card_number(rnd, prefixList, length, howMany):
  result = []
  while len(result) < howMany:
    ccnumber = copy.copy(rnd.choice(prefixList))
    result.append(completed_number(ccnumber, length))
  return result

def rangetime(): #print time.mktime((2016,1,1,0,0,0,0,0,0))
    nowTime = datetime.now().strftime("%Y%m%d%H%M%S")  # 生成当前的时间
    randomNum = random.randint(1451577600,1514735999)
    date_touple = time.localtime(randomNum)
    return time.strftime("%Y%m%d%H%M%S",date_touple)

csvFile2 =open('testc.csv','wb') # 设置newline，否则两行之间会空一行
writer = csv.writer(csvFile2)
list=['a','b','c']
writer.writerow(list)
num=100
generator = Random()
generator.seed()    # Seed from current time
mastercard = credit_card_number(generator, visaPrefixList, 16, num)
mastercard_to = credit_card_number(generator, visaPrefixList, 16, num)

for i in range(1, num, 1):
    writer.writerow([mastercard[i-1],mastercard_to[i-1],random.randint(1,10000)])
    # sheet.write(i, 0, mastercard[i-1])#债账号
    # sheet.write(i, 1, mastercard_to[i-1])#账户名称
    # sheet.write(i, 2, random.randint(1,10000)) #客户账号类型
csvFile2.close()
