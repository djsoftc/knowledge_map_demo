# -*- coding: utf-8 -*-
import networkx as nx
from networkx.algorithms import community

import matplotlib.pyplot as plt

# G = nx.read_gml('dolphins.gml')

G = nx.Graph()   # or DiGraph, MultiGraph, MultiDiGraph, etc
G.add_weighted_edges_from([('a', 'b', 3.0), ('b', 'c', 7.5), ('c', 'd', 7.5), ('m', 'e', 7.5), ('n', 'a', 7.5), ('n', 'c', 7.5)])
# G.add_weighted_edges_from(edge_list)
# G.add_edges_from([('a', 'b'), ('b', 'c'), ('c', 'd'), ('f', 'e')])
aa=[{'name':'a'},{'name':'b'},{'name':'c'},{'name':'d'},{'name':'e'},{'name':'m'},{'name':'n'}]
klist = list(community.kernighan_lin_bisection(G,weight='weight')) #list of k-cliques in the network. each element contains the nodes that consist the clique.
print klist
tempbb=[]
for index,a in enumerate(klist):
    print index
    for b in a:
        print b
        for aaa in aa:
           if aaa['name']==b:
               aaa['id']=index
               break
print aa

#plotting
# pos = nx.spring_layout(G)
# plt.clf()
# nx.draw(G,pos = pos, with_labels=False)
# nx.draw(G,pos = pos, nodelist = klist[0], node_color = 'b')
# nx.draw(G,pos = pos, nodelist = klist[1], node_color = 'y')
# plt.show()
