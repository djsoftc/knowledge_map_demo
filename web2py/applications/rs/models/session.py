import time,os
t0 = time.time()
if session.authorized:
    if session.last_time and session.last_time < t0 - auth.settings.expiration:
        session.flash = T('session expired')
        session.authorized = False
    else:
        session.last_time = t0

if not session.authorized  and not \
    (request.controller + '/' + request.function in
     ('admin/login','api_v1/'+request.function)):
    redirect(URL('admin', 'login'))

response.generic_patterns = ['*'] #if request.is_local else []

is_not_empty = IS_NOT_EMPTY(error_message=auth.messages.is_empty)
db._common_fields =[Field('create_by', length=100, default=auth.user_id, writable=False, readable=False,
                               label='创建者'),
                         Field('create_date', 'datetime', default=request.now, writable=False, readable=False,
                               label='创建时间'),
                         Field('update_by', length=100, default=auth.user_id, update=auth.user_id, writable=False,
                               readable=False, label='更新者'),
                         Field('update_date', 'datetime', default=request.now, update=request.now, writable=False,
                               readable=False, label='更新时间'),
                         Field('remarks', length=255, default='', label='备注信息'),
                         Field('del_flag', length=1, default='1',writable=False,readable=False, label='记录状态')]


db.define_table('sys_menu',
                Field('parent_id',length=64, default='',label='父级编号'),
                Field('name',length=100, default='',label='名称',requires=is_not_empty),
                Field('sortno','integer', default=0, label='排序',requires=IS_INT_IN_RANGE(0, 10 ** 3)),
                Field('href', length=2000, default='', label='链接'),
                Field('target', length=100, default='navTab', label='目标',requires=IS_IN_SET(['navTab', 'modal'])),
                Field('icon', length=100, default='', label='图标'),
                Field('is_show','boolean', default=True, label='是否在菜单中显示'),
                Field('permission', length=200, default='', label='权限标识'),
                format='%(name)s (%(id)s)'
                )
db.sys_menu.parent_id.requires = IS_EMPTY_OR(IS_IN_DB(db,db.sys_menu.id, '%(name)s',zero=T( 'choose one' )))

db.define_table('sys_role_menu',
                Field('group_id','reference auth_group' ,label='角色编号'),
                Field('menu_id','reference sys_menu',label='菜单编号'),
                )

db.define_table('sys_dict',
                Field('enumvalue',length=100, default='',label='数据值',requires=is_not_empty),
                Field('enumlabel',length=100, default='',label='标签名',requires=is_not_empty),
                Field('enumtype',length=100, default='',label='类型',requires=is_not_empty),
                Field('description',length=100, default='',label='描述',requires=is_not_empty),
                Field('sortno','integer', default=0, label='排序',requires=IS_INT_IN_RANGE(0, 10 ** 3)),
                Field('parent_id', length=64, default='0', label='父级编号'),
                )



db.define_table('app_user',
                Field('openId',length=200, default='',label='用户id'),
                Field('province',length=200, default='',label='省'),
                Field('languages', length=200, default='', label='语言'),
                Field('city', length=200, default='', label='城市'),
                Field('gender', length=200, default='', label='性别'),
                Field('avatarUrl',length=500, default='',label='头像'),
                Field('country',length=200, default='',label='国家'),
                Field('nickName',length=100, default='',label='名称'),
                Field('watermark',length=500, default='',label='水印'),
                Field('name',length=200, default='',label='名称'),
                Field('mail',length=200, default='',label='用户邮箱'),
                Field('tel',length=200, default='',label='电话'),
                Field('password',length=200, default='',label='密码'))


db.define_table('app_token',
                Field('appid',length=50, default=None,label='appid'),
                Field('appsecret',length=50, default=None,label='appsecret'),
                Field('expires_in', 'datetime'),
                Field('access_token',length=512, default=''))

db.define_table('app_slide',
                Field('title',length=500, default=None,label='图片标题',requires=is_not_empty),
                Field('img', 'upload',autodelete=True,uploadfolder=os.path.join(request.folder,'static/uploads'),label='上传图片'),
                Field('url',length=512, default='',label='图片url'),
                Field('sortno','integer', default=0, label='排序',requires=IS_INT_IN_RANGE(0, 10 ** 3)),
                Field('is_show','boolean', default=True, label='是否显示'),
                Field('wxappid', length=50, default=None, label='备用字段'))

db.define_table('app_info',
                Field('name',length=500, default=None,label='店名',requires=is_not_empty),
                Field('tel',length=20, default='',label='电话'),
                Field('address',length=200, default='',label='地址'),
                Field('longitude',length=50, default='',label='经度'),
                Field('latitude',length=50, default='',label='纬度'),
                Field('intro',length=512, default='',label='展示信息预留'),
                Field('wxappid', length=50, default=None, label='备用字段'))

db.define_table('app_type',
                Field('name',length=500, default=None,label='类型名称',requires=is_not_empty),
                Field('img', 'upload',autodelete=True,uploadfolder=os.path.join(request.folder,'static/uploads'),label='图标'),
                Field('sortno', 'integer', default=0, label='排序', requires=IS_INT_IN_RANGE(0, 10 ** 3)),
                Field('is_show', 'boolean', default=True, label='是否显示'),
                Field('url', length=512, default='', label='图片url'),
                Field('is_type',length=512, default='',label='备用类型'),
                Field('wxappid', length=50, default=None, label='备用字段'),
                format='%(name)s (%(id)s)')

db.define_table('app_photogroup',
                Field('name',length=500, default=None,label='展示名称',requires=is_not_empty),
                Field('img', 'upload',autodelete=True,uploadfolder=os.path.join(request.folder,'static/uploads'),label='展示图片'),
                Field('sortno', 'integer', default=0, label='排序', requires=IS_INT_IN_RANGE(0, 10 ** 3)),
                Field('is_show', 'boolean', default=True, label='是否显示'),
                Field('intro',length=512, default='',label='展示信息描述'),
                Field('url', length=512, default='', label='图片url'),
                Field('is_type',length=512, default='',label='备用类型'),
                Field('wxappid', length=50, default=None, label='备用字段'),
                format='%(name)s (%(id)s)')

db.define_table('app_photo',
                Field('photogroupid','reference app_photogroup' ,label='展示组别'),
                Field('name',length=500, default=None,label='名称',requires=is_not_empty),
                Field('picurl', 'upload',autodelete=True,uploadfolder=os.path.join(request.folder,'static/uploads'),label='展示图片'),
                Field('sortno', 'integer', default=0, label='排序', requires=IS_INT_IN_RANGE(0, 10 ** 3)),
                Field('is_show', 'boolean', default=True, label='是否显示'),
                Field('info',length=512, default='',label='图片描述'),
                Field('url', length=512, default='', label='图片url'),
                Field('wxappid', length=50, default=None, label='备用字段'))

db.define_table('app_artical',
                Field('typeid','reference app_type' ,label='展示组别'),
                Field('title',length=100, default=None,label='名称',requires=is_not_empty),
                Field('info',length=100, default=None,label='简介'),
                Field('img', 'upload',autodelete=True,uploadfolder=os.path.join(request.folder,'static/uploads'),label='展示图片'),
                Field('details','text', default=None,label='详细介绍'),
                Field('sortno', 'integer', default=0, label='排序', requires=IS_INT_IN_RANGE(0, 10 ** 3)),
                Field('is_show', 'boolean', default=True, label='是否显示'),
                Field('toppic', length=512, default='', label='图片url'),
                Field('wxappid', length=50, default=None, label='备用字段'))


db.define_table('app_uploadfile',
                Field('img', 'upload',autodelete=True,uploadfolder=os.path.join(request.folder,'static/uploads/tmp'),label='图片')
                )